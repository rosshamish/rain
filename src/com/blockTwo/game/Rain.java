package com.blockTwo.game;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.TimeUtils;
import java.util.ArrayList;
import java.util.Iterator;

public class Rain implements ApplicationListener {

    private OrthographicCamera camera;
    
    private SpriteBatch batch;
    
    private Texture dropImage;
    private ArrayList<Rectangle> raindrops = new ArrayList<Rectangle>();
    
    private Texture bucketImage;
    private Rectangle bucket;
    
    private Sound dropSound;
    private Music rainMusic;
    
    private long lastDropTime;
    
    @Override
    public void create() {
        /**
         * Camera.
         */
        camera = new OrthographicCamera();
        camera.setToOrtho(false, 800, 480);
        
        /**
         * Sprite Batch.
         */
        batch = new SpriteBatch();
        
        /**
         * Drop and Bucket Fields.
         */
        dropImage = new Texture(Gdx.files.internal("assets/drop.png"));
        raindrops = new ArrayList<Rectangle>();
        spawnRaindrop();
        
        bucketImage = new Texture(Gdx.files.internal("assets/bucket.png"));
        bucket = new Rectangle();
        bucket.width = 48;
        bucket.height = 48;
        bucket.x = camera.viewportWidth / 2 - bucket.width / 2;
        bucket.y = 20;
        
        /**
         * Audio.
         */
        dropSound = Gdx.audio.newSound(Gdx.files.internal("assets/drop.wav"));
        rainMusic = Gdx.audio.newMusic(Gdx.files.internal("assets/rain.mp3"));
        
        // Might as well start playing the music now.
        rainMusic.setLooping(true);
        rainMusic.play();
    }

    @Override
    public void resize(int i, int i1) {

    }

    @Override
    public void render() {
        /**
         * Screen.
         */
        // Clear
        Gdx.gl.glClearColor(0, 0, 0.2f, 1);
        Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
        
        /**
         * Camera.
         */
        // Update
        camera.update();
        
        /**
         * Drawing.
         */
        // Begin
        batch.setProjectionMatrix(camera.combined); // Use the coordinate system specified by the camera
        batch.begin();
        
        batch.draw(bucketImage, bucket.x, bucket.y, bucket.width, bucket.height);
        for (Rectangle raindrop : raindrops) {
            batch.draw(dropImage, raindrop.x, raindrop.y, raindrop.width, raindrop.height);
        }
        
        batch.end();
        // End
        
        /**
         * Handle Input.
        */
        // Mouse-Click or Touch
        if (Gdx.input.isTouched()) {
            Vector3 touchPos = new Vector3();
            touchPos.set(Gdx.input.getX(), Gdx.input.getY(), 0);
            camera.unproject(touchPos);
            bucket.x = touchPos.x - bucket.width / 2;
        }
        // Keyboard
        if (Gdx.input.isKeyPressed(Keys.LEFT) || Gdx.input.isKeyPressed(Keys.A)) {
            bucket.x -= 200 * Gdx.graphics.getDeltaTime(); // getDeltaTime() returns the time in between this frame and the last one
        }
        if (Gdx.input.isKeyPressed(Keys.RIGHT) || Gdx.input.isKeyPressed(Keys.D)) {
            bucket.x += 200 * Gdx.graphics.getDeltaTime();
        }
        // Off-Screen Handling
        if (bucket.x < 0) {
            bucket.x = 0;
        }
        if (bucket.x > camera.viewportWidth - bucket.width) {
            bucket.x = camera.viewportWidth - bucket.width;
        }
        
        /**
         * Raindrop Spawning.
         */
        // If Can Spawn, then Spawn
        if (TimeUtils.nanoTime() - lastDropTime > MathUtils.random(5, 20) * 100000000) spawnRaindrop();
        // Move the Raindrops
        Iterator<Rectangle> iter = raindrops.iterator();
        while (iter.hasNext()) {
            Rectangle raindrop = iter.next();
            raindrop.y -= 200 * Gdx.graphics.getDeltaTime();
            if (raindrop.y + raindrop.height < 0) { 
                iter.remove();
            }
            if (raindrop.overlaps(bucket)) {
                dropSound.play();
                iter.remove();
            }
        }
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void dispose() {
        batch.dispose();
        dropImage.dispose();
        bucketImage.dispose();
        dropSound.dispose();
        rainMusic.dispose();
    }
    
    public void spawnRaindrop() {
        Rectangle raindrop = new Rectangle();
        raindrop.width = 48;
        raindrop.height = 48;
        raindrop.x = MathUtils.random(0, camera.viewportWidth - raindrop.width);
        raindrop.y = camera.viewportHeight;
        raindrops.add(raindrop);
        lastDropTime = TimeUtils.nanoTime();
    }
}
