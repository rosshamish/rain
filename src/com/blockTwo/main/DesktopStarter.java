package com.blockTwo.main;

import com.badlogic.gdx.backends.jogl.JoglApplication;
import com.blockTwo.game.Rain;

/**
 * @author Ross-Desktop
 */
public class DesktopStarter {

    public static void main(String[] args) {
        new JoglApplication(new Rain(),
                                                  "Raindrops",
                                                  480, 320,
                                                  false);
    }
    
}
